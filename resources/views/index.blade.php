@extends('layout.master');

@section('title')
    List Cast
@endsection

@section('content')
<a href="/cast/create" class="btn btn-success btn-sm">Tambah</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Biodata</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
            <tr>
                <td>{{ $key + 1 }}</td>
                <td>{{ $item->nama }}</td>
                <td>{{ $item->umur }}</td>
                <td>{{ $item->bio }}</td>
                <td>
                    <a href="/cast/{{ $item->id }}" class="btn btn-primary btn-sm">Detail</a>
                    <a href="/cast/{{ $item->id }}/edit" class="btn btn-primary btn-sm">Update</a>
                    <form action="cast/{{ $item->id }}" method="POST">
                        @csrf
                        @method('delete');
                        <input type="submit" class="btn btn-danger btn-sm" value="delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td>Tidak ada data</td>
            </tr>
        @endforelse
    </tbody>
  </table>
@endsection