@extends('layout.master');

@section('title')
    Edit Cast {{ $cast->nama }}
@endsection

@section('content')
<form action="/cast/{{ $cast->id }}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="title">Nama</label>
        <input type="text" value="{{ $cast->nama }}" class="form-control" name="nama" id="title" placeholder="Masukkan Nama">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="title">Umur</label>
        <input type="text" value="{{ $cast->umur }}" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Bio</label>
        <textarea name="bio" id="body" cols="30" rows="10" placeholder="Masukkan Bio">{{ $cast->bio }}</textarea>
        @error('body')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-success">Update</button>
</form>
@endsection