<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create(){
        return view('create');
    }
    public function store(Request $request){
        $validatedData = $request->validate([
            'nama' => 'required|unique:cast,nama|max:255',
            'umur' => 'required|integer',
            'bio' => 'required',
        ],
        [
            'nama.required' => 'Nama tidak boleh kosong',
            'umur.required' => 'Umur tidak boleh kosong',
            'bio.required' => 'Biodata tidak boleh kosong',
        ]);

        DB::table('cast')->insert(
            ['nama' => $request['nama'], 
            'umur' => $request['umur'],
            'bio' => $request['bio'],
            ]
        );

        return redirect('cast/create');
    }

    public function index(){
        $users = DB::table('cast')->get();

        return view('index', ['cast' => $users]);
    }

    public function show($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('show', ['cast' => $cast]);
    }

    public function edit($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('edit', ['cast' => $cast]);
    }

    public function update(Request $request, $id){
        $validatedData = $request->validate([
            'nama' => 'required|unique:cast,nama|max:255',
            'umur' => 'required|integer',
            'bio' => 'required',
        ],
        [
            'nama.required' => 'Nama tidak boleh kosong',
            'umur.required' => 'Umur tidak boleh kosong',
            'bio.required' => 'Biodata tidak boleh kosong',
        ]);

        DB::table('cast')
              ->where('id', $id)
              ->update([
                  'nama' => $request['nama'],
                    'umur' => $request['umur'],
                    'bio' => $request['bio'],
                ]);
                return redirect('/cast');
        }

    public function destroy($id){
        DB::table('cast')->where('id', '=', $id)->delete();
        return redirect('/cast');
    }
}
